import { Doughnut } from 'react-chartjs-2'
import { Jumbotron } from 'react-bootstrap'

export default function DoughnutChart({ records}) {


  let income = records.map(income => {
    if(income.type === 'Income'){
      return income.amount
      
    } else {
      return 0
    }
  }
  )


  let totalIncome = income.reduce((x, y) => {
    return x + y
  }, 0)
  

  let expense = records.map(expense => {
    if(expense.type === 'Expense'){
      return expense.amount
      
    } else {
      return 0
    }
  }
  )
 
  let totalExpense = expense.reduce((x, y) =>{
  return x + y
}, 0)

  // let sum = records.map(a => a.amount)
  // console.log(sum)
  // let total = sum.reduce((x, y) => x + y)
  // console.log(total)
  
  let balance = totalIncome-totalExpense


  return (
     <Doughnut
     data={{
       datasets: [
       {
         data: [totalIncome, totalExpense, balance],
         backgroundColor: ['green', 'red', 'orange'],
       },
       ],
       labels: [`Total Income ${totalIncome.toLocaleString()}`, `Total Expense ${totalExpense.toLocaleString()}`, `Remaining Balance ${balance.toLocaleString()}`],
     }}
     redraw={false}
     />

    )
}

