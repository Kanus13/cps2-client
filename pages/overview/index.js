import { useState, useEffect, useContext } from 'react'
import { Table, Button } from 'react-bootstrap'
import styles from '../../styles/Home.module.css'
import DoughnutChart from '../../components/DoughnutChart'
import LineGraph from '../../components/LineGraph'
import UserContext from '../../UserContext'

export default function index() {
	const { user, activeRecords, setActiveRecords } = useContext(UserContext)
  // const [activeRecords, setActiveRecords] = useState([])

  const [state, setState] = useState([])

  // useEffect(()=>{
  //   fetch(`http://localhost:4000/api/records/user/${user.id}`)
  //   .then(res => res.json())
  //   .then(data => {
  //   setActiveRecords(data)
  //   })
  // }, [])

  return (
    <main className={styles.main}>
      <div className={styles.grid}>
        <DoughnutChart records={activeRecords}/>
        <LineGraph records={activeRecords}/>
      </div>
    </main>
  )
}

// export async function getServerSideProps() {
//   const res = await fetch('http://localhost:4000/api/records');
  
//   const data = await res.json();


//   return {
//     props: {
//       data,
    
//     },
//   }
// }