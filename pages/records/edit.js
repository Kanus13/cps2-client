import { useState, useEffect, useContext } from 'react'
import { Form, Button, Alert, Jumbotron } from 'react-bootstrap'
import Router from 'next/router'
import { useRouter } from 'next/router'
import UserContext from '../../UserContext'
import styles from '../../styles/Home.module.css'
import Swal from 'sweetalert2'


export default function edit() {
  const { user } = useContext(UserContext)
  const router = useRouter()
  const { recordId } = router.query
  const [name, setName] = useState('')
  const [type, setType] = useState('')
  const [description, setDescription] = useState('')
  const [amount, setAmount] = useState('')
  const [isActive, setIsActive] = useState(false)
  const [notify, setNotify] = useState(false)


  //retrieve course information on component getting mounted
  useEffect(() => {
    fetch(`${process.env.NEXT_PUBLIC_API_URL}/records/${recordId}`)
    .then(res => res.json())
    .then(data => {
        setName(data.name)
        setType(data.type)
        setDescription(data.description)
        setAmount(data.amount)
        setIsActive(data.isActive)
        setNotify(data.notify)
    })
}, [])


useEffect(() => {
  if(name.length < 50 && description.length < 100 && type !== 'Select Category Type' &&
  amount !== 0){
      setIsActive(true)
  }else{
      setIsActive(false)
  }
}, [name, description, type, amount])

function editRecord(e){
  e.preventDefault()

  fetch(`${process.env.NEXT_PUBLIC_API_URL}/records/${recordId}`, {
      method: 'PUT',
      headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
          name: name,
          type: type,
          description: description,
          amount: amount
      })
  })
  .then(res => res.json())
  .then(data => {
      if(data===true){
        Swal.fire({
          icon: 'success',
          title: 'Record Added',
          text: 'Record Successfully Edited!',
          showConfirmButton: false,
          timer: 1500
        })
          Router.push('/records')
          // Router.reload('/records')
      }else{
          setNotify(true)
      }
  })
}

  if (user.isRegistered) {
    return (
      <>
      <main className={styles.main} className="mt-4">
      <div className={styles.grid}>
        <Form onSubmit={(e) => editRecord(e)} style={{width: '50rem'}}>
          <Form.Group controlId='exampleForm.SelectCustom'>
            <Form.Label>Category Type:</Form.Label>
            <Form.Control
              as='select'
              custom
              value={type}
              onChange={(e) => setType(e.target.value)}
              required
            >
              <option>Select Category Type</option>
              <option>Income</option>
              <option>Expense</option>
            </Form.Control>
          </Form.Group>
          <Form.Group>
          <Form.Label>Category Name:</Form.Label>
              <Form.Control
                placeholder='Category name'
                type='text'
                value={name}
                onChange={(e) => setName(e.target.value)}
                required
              />
            </Form.Group>
                    <Form.Group>
                        <Form.Label>Amount:</Form.Label>
                        <Form.Control type="number" value={amount} onChange={e => setAmount(e.target.value)} required />
                    </Form.Group>
            <Form.Group>
                        <Form.Label>Description:</Form.Label>
                        <Form.Control as="textarea" rows="3" value={description} onChange={e => setDescription(e.target.value)} required />
                        {description.length >= 50 ? <Alert variant="warning">Description has exceeded maximum length</Alert>:null}
                    </Form.Group>

          {isActive === true ? (
            <Button type='submit' variant='success'>
              Edit Record
            </Button>
          ) : (
            <Button disabled type='submit' variant='success'>
              Edit Record
            </Button>
          )}
        </Form>

        {notify === true ? (
          <Alert variant='danger'>Sorry! Failed to edit Record!</Alert>
        ) : null}
         </div>
    </main>
      </>
    )
  } else {
    return (
      <Jumbotron>
        <h1>You are not authorized to access this page.</h1>
      </Jumbotron>
    )
  }
}
