import { useState, useEffect, useContext } from 'react'
import { Jumbotron, Row, Col } from 'react-bootstrap'
import UserRecord from '../../components/UserRecord'
import UserContext from '../../UserContext'
import Router from 'next/router'

export default function index() {

  const { user, setUser, activeRecords, setActiveRecords } = useContext(UserContext)
  const [state, setState] = useState(false)

  console.log(setUser)

  // useEffect(() => {
  //   if (activeRecords.length >= 0) {
  //     setState(true)
  //   } else{
  //     setState(false)
  //   }
  // }, [activeRecords])

  if (activeRecords.length === 0) {
    return (
      <Jumbotron>
      <h1 style={{textAlign: 'center'}}>There are no available records yet!</h1>
      <a href="/records/create">Create Record</a>
      </Jumbotron>
      )
  } else {
    if (user.isRegistered === true) {
      return (<UserRecord records={activeRecords}/>)
    } else {
      return (
        <Jumbotron>
        <h1>There are no records as of the moment.</h1>
        </Jumbotron>
 
)
        
    }
  }
}

// export async function getServerSideProps() {
//   console.log(user)
//   const res = await fetch(`http://localhost:4000/api/users/${data._id}/records`);
  
//   const data = await res.json();


//   return {
//     props: {
//       data,
    
//     },
//   }
// }