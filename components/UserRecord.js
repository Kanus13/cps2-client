import {useState, useEffect} from 'react'
import { Card, Row, Col, Button, Container } from 'react-bootstrap'
import styles from '../styles/Home.module.css'
import moment from 'moment'
import UpdateButton from '../components/UpdateButton'
import ArchiveButton from '../components/ArchiveButton'

export default function UserRecord({records}){

	const [isActive, setIsActive] = useState(false)

	useEffect(()=>{
		if(records.length >= 0){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	})

	let income = records.map(income => {
		if(income.type === 'Income'){
			return income.amount
			
		} else {
			return 0
		}
	}
	)
	

	let totalIncome = income.reduce((x, y) => {
		return x + y
	}, 0)


	let expense = records.map(expense => {
		if(expense.type === 'Expense'){
			return expense.amount
			
		} else {
			return 0
		}
	}
	)

	let totalExpense = expense.reduce((x, y) => {
		return x + y
	}, 0)

	// let sum = records.map(a => a.amount)
	// console.log(sum)
	// let total = sum.reduce((x, y) => x + y)
	// console.log(total)
	
	let total = totalIncome-totalExpense
	if(isActive === true){
	return(
		<>
		
		<main className={styles.main}>
		
		
		<div className={styles.grid}>
		
			<Col className={styles.recordcard}>
		<h1>Records</h1>
		<Button variant="primary" href="/records/create">+ Add Record</Button>
				
			</Col>
			<Col>
				
		<h5 style={{color: 'orange'}}>Remaining Balance: {total.toLocaleString()}</h5>
		<h6 style={{color: 'green'}}>Total Income: {totalIncome.toLocaleString()}</h6>
		<h6 style={{color: 'red'}}>Total Expenses: {totalExpense.toLocaleString()}</h6>
			</Col>
	
        
		{records.map( record => {

			return (
		<>
				<Row key={record._id}>
				<Card className={styles.recordcard}>
				<Card.Body>
				<Row>
				<Col>
				<Card.Title style={{fontFamily: "Fira Code"},{fontSize: '2rem'}}>{record.name}</Card.Title>
				{record.type === 'Income'
				?<Card.Subtitle>
				<span style={{color: 'green'}}>{record.type}</span> 
				<span>&nbsp;({record.description})</span>
				</Card.Subtitle>
				:<Card.Subtitle>
				<span style={{color: 'red'}}>{record.type}</span> 
				<span>&nbsp;({record.description})</span>
				</Card.Subtitle>

			}
			<Card.Subtitle className="mt-1">{moment(record.createdOn).format('lll')}
			</Card.Subtitle>
			</Col>
			<Col>
			{
			
			record.type === 'Income'
			? <Card.Text className={styles.income}>+ {record.amount.toLocaleString()}
				<UpdateButton recordId={record._id}/>
				<ArchiveButton recordId={record._id}/>
			</Card.Text>
			: <Card.Text className={styles.expense}>- {record.amount.toLocaleString()}
			<UpdateButton recordId={record._id}/>
			<ArchiveButton recordId={record._id}/>
			
			</Card.Text>
		
		}

		</Col>
		</Row>
		</Card.Body>
		</Card>
		</Row>
		</>
		)
	}).reverse()}
	</div>
	</main>
	</>
	)} else {
				return null
			}
}