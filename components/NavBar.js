import { useContext } from 'react'
import { Navbar, Nav, Image } from 'react-bootstrap'
import Link from 'next/link'
import UserContext from '../UserContext'

export default function NavBar() {
  const { user } = useContext(UserContext)
  return (
    <Navbar bg='light' expand='lg'>
      <Navbar.Brand href='/'>
        <Image src='https://img.icons8.com/color/48/000000/budget.png' /> Magastos
      </Navbar.Brand>
      <Navbar.Toggle aria-controls='basic-navbar-nav' />
      <Navbar.Collapse id='basic-navbar-nav'>
        <Nav className='ml-auto'>
          {user.id !== null ? (
            user.isRegistered === true ? (
              <>
                <Link href='/records'>
                  <a className='nav-link' role='button'>
                    Records
                  </a>
                </Link>
                <Link href='/categories'>
                  <a className='nav-link' role='button'>
                    Categories
                  </a>
                </Link>
                <Link href='/overview'>
                  <a className='nav-link' role='button'>
                    Overview
                  </a>
                </Link>
                <Link href='/logout'>
                  <a className='nav-link' role='button'>
                    Logout
                  </a>
                </Link>
              </>
            ) : (
              <Link href='/logout'>
                <a className='nav-link' role='button'>
                  Logout
                </a>
              </Link>
            )
          ) : (
            <>
              <Link href='/register'>
                <a className='nav-link' role='button'>
                  Register
                </a>
              </Link>
              <Link href='/login'>
                <a className='nav-link' role='button'>
                  Login
                </a>
              </Link>
            </>
          )}
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  )
}
