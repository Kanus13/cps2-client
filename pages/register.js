import { useState, useEffect } from 'react'
import {
  Form,
  Button,
  Row,
  Col,
  InputGroup,
  FormControl,
  Alert,
} from 'react-bootstrap'
import Router from 'next/router'
import styles from '../styles/Home.module.css'
import Swal from 'sweetalert2'

export default function register() {
  //form input state hooks
  const [email, setEmail] = useState('')
  const [password1, setPassword1] = useState('')
  const [password2, setPassword2] = useState('')
  const [firstName, setFirstName] = useState('')
  const [lastName, setLastName] = useState('')
  const [mobileNumber, setMobileNumber] = useState('')
  const [isActive, setIsActive] = useState(false)

  useEffect(() => {
    //additional validations for the password
    if (
      password1.length >= 8 &&
      password1.length <= 20 &&
      password1 !== '' &&
      password2 !== '' &&
      password2 === password1 &&
      mobileNumber.length === 10
      ) {
      setIsActive(true)
  } else {
    setIsActive(false)
  }
}, [password1, password2, mobileNumber])

  function registerUser(e) {
    e.preventDefault()

    fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/register`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        firstName: firstName,
        lastName: lastName,
        email: email,
        mobileNumber: mobileNumber,
        password: password1,
      }),
    })
    .then((res) => res.json())
    .then((data) => {
      if (data === true) {
        Swal.fire({
          icon: 'success',
          title: 'Record Added',
          text: 'Registration Successful!',
          showConfirmButton: false,
          timer: 1500
        })
        Router.push('/login')
      } else {
        Router.push('/error')
      }
    })
  }

  return (
    <main className={styles.main}>
    <div className={styles.grid}>
    <h2 className={styles.registerHeader}>Create an account</h2>
    <Form onSubmit={(e) => registerUser(e)} style={{ width: '50rem' }}>
    <Form.Row>
    <Form.Group as={Col}>
    <Form.Control
    placeholder='First name'
    type='text'
    value={firstName}
    onChange={(e) => setFirstName(e.target.value)}
    required
    />
    </Form.Group>
    <Form.Group as={Col}>
    <Form.Control
    placeholder='Last name'
    type='text'
    value={lastName}
    onChange={(e) => setLastName(e.target.value)}
    required
    />
    </Form.Group>
    </Form.Row>
    <Form.Group>
    <Form.Label>Mobile number</Form.Label>
    <Form.Label htmlFor='inlineFormInputGroupUsername' srOnly>
    Mobile Number
    </Form.Label>
    <InputGroup>
    <InputGroup.Prepend>
    <InputGroup.Text>+63</InputGroup.Text>
    </InputGroup.Prepend>
    <FormControl
    id='inlineFormInputGroupUsername'
    placeholder='9xxxxxxxxx'
    type='text'
    value={mobileNumber}
    aria-describedby='mobnumber'
    onChange={(e) => setMobileNumber(e.target.value)}
    required
    />
    </InputGroup>
    <Form.Text id='mobnumber' muted>
    {mobileNumber.length === 10 ? null : (
      <Alert variant='warning'>
      Mobile Number input must be 10 characters long only
      </Alert>
      )}
    </Form.Text>
    </Form.Group>
    <Form.Group>
    <Form.Label>Email</Form.Label>
    <Form.Control
    type='email'
    value={email}
    onChange={(e) => setEmail(e.target.value)}
    required
    />
    </Form.Group>
    <Form.Row>
    <Form.Group as={Col}>
    <Form.Label>Password</Form.Label>
    <Form.Control
    type='password'
    value={password1}
    onChange={(e) => setPassword1(e.target.value)}
    required
    id='inputPassword5'
    aria-describedby='passwordHelpBlock'
    />
    <Form.Text id='passwordHelpBlock' muted>
    Your password must be 8-20 characters long, contain letters and
    numbers, and must not contain spaces, special characters, or
    emoji.
    </Form.Text>
    </Form.Group>
    <Form.Group as={Col}>
    <Form.Label>Verify Password</Form.Label>
    <Form.Control
    type='password'
    value={password2}
    onChange={(e) => setPassword2(e.target.value)}
    required
    />
    </Form.Group>
    </Form.Row>
    {isActive === true ? (
      <Button variant='success' type='submit' block>
      Submit
      </Button>
      ) : (
      <Button variant='success' type='submit' block disabled>
      Register
      </Button>
      )}
      </Form>
      </div>
      </main>
      )
}
