import { useState, useEffect } from 'react'
import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import { UserProvider } from '../UserContext'
import { Container } from 'react-bootstrap'
import NavBar from '../components/NavBar'
import Head from 'next/head'

function MyApp({ Component, pageProps }) {
  const [activeRecords, setActiveRecords] = useState([])
  const [activeCategories, setActiveCategories] = useState([])
  const [user, setUser] = useState({
    id: null,
    firstName: '',
    isRegistered: null,
  })

  const unsetUser = () => {
  
    localStorage.clear()

    setUser({
      id: null,
      firstName: '',
      isRegistered: null,
    })
  }


  useEffect(() => {
    fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/details`, {
      headers: {
    
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
     
      .then((res) => res.json())
      .then((data) => {
      // console.log(data, 'data')
        if (data._id) {

          setUser({
            id: data._id,
            firstName: data.firstName,
            isRegistered: data.isRegistered
          })
          fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/${data._id}/records`).then(res => res.json()).then(data => {
            setActiveRecords(data)
          })
          fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/${data._id}/categories`).then(res => res.json()).then(data => {
            setActiveCategories(data)
          })
          
        } else {

          setUser({
            id: null,
            firstName: '',
            isRegistered: null,
          })
        }
      })
  }, [user.id, user.isRegistered])

  return (
    
    <>
      <Head>
        <title>Magastos App</title>
      </Head>
    
      <UserProvider value={{ user, setUser, unsetUser, activeRecords, activeCategories, setActiveCategories }}>
        <NavBar />
        <Container>
          <Component {...pageProps} />
        </Container>
      </UserProvider>
    </>
  )
}

export default MyApp
