import { Button } from 'react-bootstrap'
import Router from 'next/router'

export default function UpdateButton({recordId}) {
    const dirToEditForm = (recordId) => {
        Router.push({
            pathname: '/records/edit',
            query: {recordId: recordId}
        })
    }
    return (
        <Button className="ml-2" variant="warning" onClick={() => dirToEditForm(recordId)}>Update</Button>
    )
}
