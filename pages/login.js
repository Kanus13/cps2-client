import Head from 'next/head'
import Link from 'next/link'
import styles from '../styles/Home.module.css'
import { Card, Button, Image, Form } from 'react-bootstrap'
import { GoogleLogin } from 'react-google-login'
import Swal from 'sweetalert2'
import { useState, useContext } from 'react'
import UserContext from '../UserContext'
import Router from 'next/router'

export default function login() {
  const { setUser } = useContext(UserContext)
  //states for form input
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [isMatched, setIsMatched] = useState(true)

  const captureLoginResponse = (response) => {
    // console.log(response)
    const payload = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ tokenId: response.tokenId }),
    }

    fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/verify-google-id-token`, payload)
      .then((response) => response.json())
      .then((data) => {
        if (typeof data.accessToken !== 'undefined') {
          localStorage.setItem('token', data.accessToken)
          setUser({ id: data._id, isRegistered: data.isRegistered })
          Router.push('/records')
        } else {
          if (data.error == 'google-auth-error') {
            Swal.fire(
              'Google Auth Error',
              'Google authentication procedure failed.',
              'error'
            )
          } else if (data.error === 'login-type-error')
            Swal.fire(
              'Login Type Error',
              'You may have registered through a different login procedure.',
              'error'
            )
        }
      })
  }

  function authenticate(e) {
    e.preventDefault()

    fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.accessToken) {
          localStorage.setItem('token', data.accessToken)

          fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/details`, {
            headers: {
              Authorization: `Bearer ${data.accessToken}`,
            },
          })
            .then((res) => res.json())
            .then((data) => {
              setUser({
                id: data._id,
                isRegistered: data.isRegistered,
              })
              Router.push('/records')
            })
        } else {
          setIsMatched(false)
          setEmail('')
          setPassword('')
        }
      })
  }

  return (
    <div className={styles.container}>
      <Head>
        <title>Magastos</title>
        <link
          rel='icon'
          href='https://img.icons8.com/color/48/000000/budget.png'
        />
      </Head>

      <main className={styles.main} className="mt-4">
        <div className={styles.grid}>
          <Card style={{ width: '20rem' }}>
            <div className={styles.logocenter}>
              <Image className={styles.circle} src='circle-logo.png' />
            </div>
            {isMatched === false ? (
              <Form.Text className='text-muted text-center'>
                <p className={styles.invalid}>Invalid Credentials!</p>
              </Form.Text>
            ) : null}
            <Card.Body>
              <Form onSubmit={(e) => authenticate(e)}>
                <Form.Group controlId='formBasicEmail'>
                  <Form.Label>Email address</Form.Label>
                  <Form.Control
                    type='email'
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    placeholder='Enter email'
                  />
                </Form.Group>

                <Form.Group controlId='formBasicPassword'>
                  <Form.Label>Password</Form.Label>
                  <Form.Control
                    type='password'
                    placeholder='Enter your password'
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                  />
                </Form.Group>
                <Button variant='primary' type='submit' block>
                  Login
                </Button>
              </Form>
              <p style={{ textAlign: 'center' }}>or</p>
              <GoogleLogin
                clientId='626268025921-0ig4bpf99quft9h4vtde3idvufi8ptli.apps.googleusercontent.com'
                buttonText='Login using Google Account'
                onSuccess={captureLoginResponse}
                onFailure={captureLoginResponse}
                icon={true}
                theme='dark'
                className={styles.btn}
                cookiePolicy={'single_host_origin'}
              />
              <div className={styles.registerHere}>
                <p>
                  Don't have an account yet? <Link href="/register"><a>Register</a></Link>{' '}
                  here
                </p>
              </div>
            </Card.Body>
          </Card>
        </div>
      </main>
    </div>
  )
}
