import { Table, Button } from 'react-bootstrap'
import styles from '../styles/Home.module.css'
import UpdateCategory from '../components/UpdateCategory';

export default function UserCategory({categories}) {
  return (
    <main className={styles.main}>
      <div className={styles.grid}>
      <h1>Categories</h1>
    <Button variant="primary" href="/categories/create">+ Add Category</Button>
        <Table
          striped
          bordered
          hover
          style={{ width: '50rem' }}
          className='mt-4'
        >
          <thead>
            <tr>
              <th className='text-center' className={styles.thead}>
                Category Name
              </th>
              <th className='text-center' className={styles.thead}>
                Category Type
              </th>
            </tr>
          </thead>
          <tbody>
             {categories.map( category => {

      return (
            <tr>
              <td className='text-center' className={styles.thead}>
                {category.name}
              </td>
              <td className='text-center' className={styles.thead}>
                {category.type}
              </td>
              <td>
                <UpdateCategory categoryId={category._id}/>
              </td>
            </tr>
            )}).reverse()}
          </tbody>
        </Table>
      </div>
    </main>
  )
}
