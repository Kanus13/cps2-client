import Head from 'next/head'
import styles from '../styles/Home.module.css'
import { Card, Button, Image, Form } from 'react-bootstrap'
import GoogleLogin from 'react-google-login'

export default function Home() {
  return (
      <>
    <div className={styles.container}>
      <Head>
        <title>Magastos App</title>
        <link
          rel='icon'
          href='https://img.icons8.com/color/48/000000/budget.png'
        />
      </Head>

        <h1>Magastos</h1>
        <div>
          
        <h4>Track how you spend like a one-day-millionaire</h4>
        </div>
      <main className={styles.main}>
        <div className={styles.grid}>
          
        </div>
      </main>
    </div>
    </>
    )
}
