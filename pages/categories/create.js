import { useState, useEffect, useContext } from 'react'
import { Form, Button, Alert, Jumbotron } from 'react-bootstrap'
import Router from 'next/router'
import UserContext from '../../UserContext'
import Swal from 'sweetalert2'

export default function create() {
  const { user } = useContext(UserContext)
  const [name, setName] = useState('')
  const [type, setType] = useState('')
  const [isActive, setIsActive] = useState(false)
  const [notify, setNotify] = useState(false)

  useEffect(() => {
    if (user.isRegistered === true) {
      Router.push('/categories')
    }
  }, [])

  useEffect(() => {
    if (name.length < 30 && name.length > 1 && type !== 'Select Category Type') {
      setIsActive(true)
    } else {
      setIsActive(false)
    }
  }, [name, type])

  function createCategory(e) {
    e.preventDefault()

    fetch(`${process.env.NEXT_PUBLIC_API_URL}/categories`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        name: name,
        type: type,
      }),
    })
    .then((res) => res.json())
    .then((data) => {
      if (data === true) {
        Swal.fire({
          icon: 'success',
          title: 'Record Added',
          text: 'Category Successfully Created!',
          showConfirmButton: false,
          timer: 1500
        })
        Router.push('/categories')
        // Router.reload('/categories')
      } else {
        setNotify(true)
      }
    })
  }

  if (user.isRegistered) {
    return (
      <>
      <Form onSubmit={(e) => createCategory(e)}>
        <Form.Group controlId='exampleForm.SelectCustom'>
        <Form.Label>Category Type:</Form.Label>
        <Form.Control
        as='select'
        custom
        value={type}
        onChange={(e) => setType(e.target.value)}
          required
          >
          <option>Select Category Type</option>
          <option>Income</option>
          <option>Expense</option>
          </Form.Control>

          </Form.Group>
          <Form.Group>
          <Form.Label>Category Name:</Form.Label>
          <Form.Control
          type='text'
          value={name}
          onChange={(e) => setName(e.target.value)}
            required
            />
            {name.length >= 50 ? (
              <Alert variant='warning'>Category Name has exceeded maximum length</Alert>
              ) : null}
              </Form.Group>
              {isActive === true ? (
                <Button type='submit' variant='success'>
                Create Category
                </Button>
                ) : (
                  <Button disabled type='submit' variant='success'>
                  Create Category
                  </Button>
                  )}
                  </Form>

                  {notify === true 
                    ? 
                    Swal.fire({
                      icon: 'error',
                      title: 'Oops...',
                      text: 'Ooopss! Something went wrong!'
                    })
                    : null}
                    </>
                    )
              } else {
                return (
                  <Jumbotron>
                  <h1>You are not authorized to access this page.</h1>
                  </Jumbotron>
                  )
                }
              }
