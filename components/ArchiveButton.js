import { Button } from 'react-bootstrap'
import Router from 'next/router'

export default function ArchiveButton({recordId}) {

    const archive = (recordId) => {
        fetch(`${process.env.NEXT_PUBLIC_API_URL}/records/${recordId}`, {
            method: 'DELETE',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            if(data === true){
                Router.reload()
            }else{
                Router.push('/error')
            }
        })
    }
    return (<Button variant="danger" onClick={()=>archive(recordId)} className="delete-btn">Archive</Button>)
    // if(isActive===true){
    //     return <Button variant="danger" onClick={()=>archive(recordId)}>Archive</Button>
    // }else{
    //     return <Button variant="success" onClick={()=>archive(recordId)}>Reactivate</Button>
    // }

}
