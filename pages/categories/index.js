import { useState, useEffect, useContext } from 'react'
import { Jumbotron, Row, Col } from 'react-bootstrap'
import UserCategory from '../../components/UserCategory'
import UserContext from '../../UserContext'

export default function index() {
   const { user, activeCategories, setActiveCategories } = useContext(UserContext)
  

  // useEffect(() => {
  //   if (data.length > 0) {
  //     setActiveCategories(data.filter((category) => category.isActive === true))
  //   }
  // }, [data])

  if (activeCategories.length === 0) {
    return (
      <Jumbotron>
        <h1 style={{textAlign: 'center'}}>There are no available categories yet!</h1>
        <a href="/categories/create">Create Category</a>
      </Jumbotron>
    )
  } else {
    if (user.isRegistered === true) {
      return <UserCategory categories={activeCategories} />
    } else {
      return (
        <Jumbotron>
          <h1>There are no categories as of the moment.</h1>
        </Jumbotron>
      )
    }
  }
}

// export async function getServerSideProps() {
//   const res = await fetch('http://localhost:4000/api/categories')
//   const data = await res.json()

//   return {
//     props: {
//       data,
//     },
//   }
// }
