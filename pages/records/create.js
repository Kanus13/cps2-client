import { useState, useEffect, useContext } from 'react'
import { Form, Button, Alert, Jumbotron } from 'react-bootstrap'
import Router from 'next/router'
import UserContext from '../../UserContext'
import styles from '../../styles/Home.module.css'
import Swal from 'sweetalert2'

export default function create() {
  const { user, activeCategories, setActiveCategories } = useContext(UserContext)
  const [name, setName] = useState('')
  const [type, setType] = useState('')
  const [description, setDescription] = useState('')
  const [amount, setAmount] = useState('')
  const [isActive, setIsActive] = useState(false)
  const [notify, setNotify] = useState(false)

  useEffect(() => {
    if (user.isRegistered === true) {
      Router.push('/records')
    }
  }, [])

  useEffect(() => {
    if (
      name.length < 50 &&
      name.length > 1 &&
      description.length < 100 &&
      description.length > 1 &&
      type !== 'Select Category Type' &&
      amount !== 0
      ) {
      setIsActive(true)
  } else {
    setIsActive(false)
  }
}, [name, description, type])

  function createRecord(e) {
    e.preventDefault()

    fetch(`${process.env.NEXT_PUBLIC_API_URL}/records`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        name: name,
        type: type,
        amount: amount,
        description: description,
      }),
    })
    .then((res) => res.json())
    .then((data) => {
      if (data === true) {
        Swal.fire({
          icon: 'success',
          title: 'Record Added',
          text: 'Congratulations! Record Successfully Added.',
          showConfirmButton: false,
          timer: 1500
        })
        Router.push('/records')
        // Router.reload('/records')
        
      } else {
        setNotify(true)
      }
    })
  }

  if (user.isRegistered) {
    return (
      <>
      <main className={styles.main} className="mt-4">
      <div className={styles.grid}>
      <Form onSubmit={(e) => createRecord(e)} style={{width: '50rem'}}>
      <Form.Group controlId='exampleForm.SelectCustom'>
      <Form.Label>Category Type:</Form.Label>
      <Form.Control
      as='select'
      custom
      value={type}
      onChange={(e) => setType(e.target.value)}
      required
      >
      <option>Select Category Type</option>
      <option>Income</option>
      <option>Expense</option>
      </Form.Control>
      </Form.Group>
      <Form.Group controlId='exampleForm.SelectCustom'>
      <Form.Label>Category Name:</Form.Label>
      <Form.Control
      placeholder='Category name'
      as='select'
      custom
      value={name}
      onChange={(e) => setName(e.target.value)}
      required
      >
      <option>Select Category Name</option>
      {activeCategories.map( category => {

        return (<option>{category.name}</option>)}).reverse()}
      </Form.Control>
      </Form.Group>
      <Form.Group>
      <Form.Label>Amount:</Form.Label>
      <Form.Control type="number" value={amount} onChange={e => setAmount(e.target.value)} required />
      </Form.Group>
      <Form.Group>
      <Form.Label>Description:</Form.Label>
      <Form.Control as="textarea" rows="3" value={description} onChange={e => setDescription(e.target.value)} required />
      {description.length >= 50 ? <Alert variant="warning">Description has exceeded maximum length</Alert>:null}
      </Form.Group>

      {isActive === true ? (
        <Button type='submit' variant='success' block>
        Create Record
        </Button>
        ) : (
        <Button disabled type='submit' variant='success' block>
        Create Record
        </Button>
        )}
        </Form>

        {notify === true ? (
          <Alert variant='danger'>Failed to create a record!</Alert>
          ) : null}
        </div>
        </main>
        </>
        )
      } else {
        return (
        <Jumbotron>
        <h1>You are not authorized to access this page.</h1>
        </Jumbotron>
        )
      }
    }


    // export async function getServerSideProps() {
    //   const res = await fetch('http://localhost:4000/api/categories')
    //   const data = await res.json()

    //   return {
    //     props: {
    //       data,
    //     },
    //   }
    // }