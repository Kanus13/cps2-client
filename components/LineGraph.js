import React from 'react';
import {Line} from 'react-chartjs-2';
import moment from 'moment'

export default function LineGraph({records}){

  let income = records.map(income => {
    if(income.type === 'Income'){
      return income.amount
      
    } else {
      return 0
    }
  }
  )

  let dateLabels = records.map(dateLabel => {
    if(records.length > 0){
      return moment(dateLabel.createdOn).format('ll')
    } else {
      return null
    }
  }
  )


  let totalIncome = income.reduce((x, y) => {
    return x + y
  }, 0)
  

  let expense = records.map(expense => {
    if(expense.type === 'Expense'){
      return expense.amount
      
    } else {
      return 0
    }
  }
  )
 
  let totalExpense = expense.reduce((x, y) =>{
    return x + y
  }, 0)

  // let sum = records.map(a => a.amount)
  // console.log(sum)
  // let total = sum.reduce((x, y) => x + y)
  // console.log(total)
  
  let balance = totalIncome-totalExpense

  return (

    <Line
    data={{
     datasets: [
     {
       label: 'Income',
       data: income,
       backgroundColor: ['green'],
       fill: false,
       lineTension: 0.1,
       backgroundColor: 'green',
       borderColor: 'green',
       borderCapStyle: 'butt',
       borderDash: [],
       borderDashOffset: 0.0,
       borderJoinStyle: 'miter',
       pointBorderColor: 'green',
       pointBackgroundColor: '#fff',
       pointBorderWidth: 1,
       pointHoverRadius: 5,
       pointHoverBackgroundColor: 'green',
       pointHoverBorderColor: 'green',
       pointHoverBorderWidth: 2,
       pointRadius: 1,
       pointHitRadius: 10,
     },
     { 
       label: 'Expenses',
       data: expense,
       backgroundColor: ['red'],
       fill: false,
       lineTension: 0.1,
       backgroundColor: 'red',
       borderColor: 'red',
       borderCapStyle: 'butt',
       borderDash: [],
       borderDashOffset: 0.0,
       borderJoinStyle: 'miter',
       pointBorderColor: 'red',
       pointBackgroundColor: '#fff',
       pointBorderWidth: 1,
       pointHoverRadius: 5,
       pointHoverBackgroundColor: 'red',
       pointHoverBorderColor: 'red',
       pointHoverBorderWidth: 2,
       pointRadius: 1,
       pointHitRadius: 10,
     },
     ],
     labels: dateLabels,
   }}
   redraw={false}
   />
   )

}