import { useState, useEffect, useContext } from 'react'
import { Form, Button, Alert, Jumbotron } from 'react-bootstrap'
import Router from 'next/router'
import { useRouter } from 'next/router'
import UserContext from '../../UserContext'
import styles from '../../styles/Home.module.css'
import Swal from 'sweetalert2'

export default function edit() {
  const { user } = useContext(UserContext)
  const router = useRouter()
  const { categoryId } = router.query
  const [name, setName] = useState('')
  const [type, setType] = useState('')
  const [isActive, setIsActive] = useState(false)
  const [notify, setNotify] = useState(false)


  //retrieve course information on component getting mounted
  useEffect(() => {
    fetch(`${process.env.NEXT_PUBLIC_API_URL}/categories/${categoryId}`)
    .then(res => res.json())
    .then(data => {
        setName(data.name)
        setType(data.type)
        setIsActive(data.isActive)
        setNotify(data.notify)
    })
}, [])


useEffect(() => {
  if(name.length < 50 && name.length > 1 && type !== 'Select Category Type'){
      setIsActive(true)
  }else{
      setIsActive(false)
  }
}, [name, type])

function editCategory(e){
  e.preventDefault()

  fetch(`${process.env.NEXT_PUBLIC_API_URL}/categories/${categoryId}`, {
      method: 'PUT',
      headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
          name: name,
          type: type
      })
  })
  .then(res => res.json())
  .then(data => {
      if(data===true){
        Swal.fire({
          icon: 'success',
          title: 'Category Added',
          text: 'Category Successfully Edited!',
          showConfirmButton: false,
          timer: 2000
        })
          Router.push('/categories')
          // Router.reload()
      }else{
          setNotify(true)
      }
  })
}

  if (user.isRegistered) {
    return (
      <>
      <main className={styles.main} className="mt-4">
      <div className={styles.grid}>
        <Form onSubmit={(e) => editCategory(e)} style={{width: '50rem'}}>
          <Form.Group controlId='exampleForm.SelectCustom'>
            <Form.Label>Category Type:</Form.Label>
            <Form.Control
              as='select'
              custom
              value={type}
              onChange={(e) => setType(e.target.value)}
              required
            >
              <option>Select Category Type</option>
              <option>Income</option>
              <option>Expense</option>
            </Form.Control>
          </Form.Group>
          <Form.Group>
          <Form.Label>Category Name:</Form.Label>
              <Form.Control
                placeholder='Category name'
                type='text'
                value={name}
                onChange={(e) => setName(e.target.value)}
                required
              ></Form.Control>
              </Form.Group>

          {isActive === true ? (
            <Button type='submit' variant='success'>
              Edit Category
            </Button>
          ) : (
            <Button disabled type='submit' variant='success'>
              Edit Category
            </Button>
          )}
        </Form>

        {notify === true ? (
          <Alert variant='danger'>Sorry! Failed to edit Category!</Alert>
        ) : null}
         </div>
    </main>
      </>
    )
  } else {
    return (
      <Jumbotron>
        <h1>You are not authorized to access this page.</h1>
      </Jumbotron>
    )
  }
}
