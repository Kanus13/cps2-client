import { Button } from 'react-bootstrap'
import Router from 'next/router'

export default function UpdateCategory({categoryId}) {
    const dirToEditForm = (categoryId) => {
        Router.push({
            pathname: '/categories/edit',
            query: {categoryId: categoryId}
        })
    }
    return (
        <Button className="ml-2" variant="warning" onClick={() => dirToEditForm(categoryId)}>Update</Button>
    )
}
